﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Books.Models {

   public class Book {
      public int Id {
         get;
         set;
      }

      [Required]
      [DisplayName("Book Title")]
      public string Title {
         get;
         set;
      }

      [Required]
      [DisplayName("Last Name")]
      public string AuthorLastName {
         get;
         set;
      }

      [Required]
      [DisplayName("First Name")]
      public string AuthorFirstName {
         get;
         set;
      }

      public string Author {
         get {
            return String.Format("{0} {1}", AuthorFirstName, AuthorLastName);
         }
      }

      [RegularExpression(@"\d{4}", ErrorMessage="Year must be a 4 digit value.")]
      public int Year {
         get;
         set;
      }
   }
}
