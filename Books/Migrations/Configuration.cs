
namespace Books.Migrations {
   using System;
   using System.Data.Entity;
   using System.Data.Entity.Migrations;
   using System.Linq;
   using Microsoft.AspNet.Identity.EntityFramework;
   using Books.Models;
   using Microsoft.AspNet.Identity;
   using System.Collections.Generic;
   internal sealed class Configuration : DbMigrationsConfiguration<Books.Models.ApplicationDbContext> {
      public Configuration() {
         AutomaticMigrationsEnabled = true;
         AutomaticMigrationDataLossAllowed = true;
         ContextKey = "Books.Models.ApplicationDbContext";
      }

      protected override void Seed(Books.Models.ApplicationDbContext context) {
         //var userStore = new UserStore<ApplicationUser>(context);
         //var userManager = new UserManager<ApplicationUser>(userStore);

         //if (!context.Users.Any(t => t.UserName == "admin@mvsatm.com")) {
         //   var user = new ApplicationUser {
         //      UserName = "admin@mvcatm.com", Email = "admin@mvcatm.com"
         //   };
         //   userManager.Create(user, "passW0rd!");
         //   context.Roles.AddOrUpdate(r => r.Name, new IdentityRole {
         //      Name = "Admin"
         //   });
         //   context.SaveChanges();
         //   userManager.AddToRole(user.Id, "Admin");
         //   //Console.WriteLine("Admin user id = {0}", user.Id);
         //}
         var books = new List<Book> {
            new Book{Title="Harry Potter and the Philosopher's Stone", AuthorLastName="Rowling", AuthorFirstName = "J.K", Year=1997},
            new Book{Title="Harry Potter and the Chamber of Secrets", AuthorLastName="Rowling", AuthorFirstName = "J.K", Year=1998},
            new Book{Title="The Girl withe the Dragon Tattoo", AuthorLastName="Larsson", AuthorFirstName="Stieg", Year=2008},
            new Book{Title="The Girl Who Played with Fire", AuthorLastName="Larsson", AuthorFirstName="Stieg", Year=2009},
            new Book{Title="Mrs McGinty's Dead", AuthorLastName="Christie", AuthorFirstName="Agatha", Year=1952},
            new Book{Title="They Do It With Mirrors", AuthorLastName="Christie", AuthorFirstName="Agatha", Year=1952},
            new Book{Title="Guide to Computer Forensics and Investigations", AuthorLastName="Nelson", AuthorFirstName="Bill", Year=2006},
            new Book{Title="The Algorithm Design Manual", AuthorLastName="Skiena", AuthorFirstName="Steven", Year=2008}
         };
         books.ForEach(b => context.Books.Add(b));
         context.SaveChanges();
      }
   }
}
