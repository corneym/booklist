﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Books.Models;
using PagedList;

namespace Books.Controllers {
   [Authorize]
   public class BookController : Controller {

      private ApplicationDbContext db = new ApplicationDbContext();

      // GET: Book
      public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page) {

         ViewBag.CurrentSort = sortOrder;

         ViewBag.TitleSortParm = String.IsNullOrEmpty(sortOrder) ? "title_desc" : "";
         ViewBag.AuthorSortParm = sortOrder == "author" ? "author_desc" : "author";
         ViewBag.YearSortParm = sortOrder == "year" ? "year_desc" : "year";

         if (searchString != null) {
            page = 1;
         } else {
            searchString = currentFilter;
         }

         ViewBag.CurrentFilter = searchString;

         var books = from b in db.Books select b;
         if (!String.IsNullOrEmpty(searchString)) {
            books = books.Where(b => b.AuthorLastName.Contains(searchString)
                                   || b.AuthorFirstName.Contains(searchString));
         }

         switch (sortOrder) {
            case "author":
               books = books.OrderBy(b => b.AuthorLastName);
               break;
            case "author_desc":
               books = books.OrderByDescending(b => b.AuthorLastName);
               break;
            case "year":
               books = books.OrderBy(b => b.Year);
               break;
            case "year_desc":
               books = books.OrderByDescending(b => b.Year);
               break;
            case "title_desc":
               books = books.OrderByDescending(b => b.Title);
               break;
            default:
               books = books.OrderBy(b => b.Title);
               break;
         }

         int pageSize = 5;
         int pageNumber = (page ?? 1);
         return View(books.ToPagedList(pageNumber, pageSize));
      }

      // GET: Book/Details/5
      public ActionResult Details(int? id) {
         if (id == null) {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
         }
         Book book = db.Books.Find(id);
         if (book == null) {
            return HttpNotFound();
         }
         return View(book);
      }

      // GET: Book/Create
      public ActionResult Create() {
         return View();
      }

      // POST: Book/Create
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult Create([Bind(Include = "Id,Title,AuthorLastName,AuthorFirstName,Year")] Book book) {
         if (ModelState.IsValid) {
            db.Books.Add(book);
            db.SaveChanges();
            return RedirectToAction("Index");
         }

         return View(book);
      }

      // GET: Book/Edit/5
      public ActionResult Edit(int? id) {
         if (id == null) {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
         }
         Book book = db.Books.Find(id);
         if (book == null) {
            return HttpNotFound();
         }
         ViewData["Id"] = id;
         return View(book);
      }

      // POST: Book/Edit/5
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult Edit([Bind(Include = "Id,Title,AuthorLastName,AuthorFirstName,Year")] Book book) {
         if (ModelState.IsValid) {
            db.Entry(book).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
         }
         return View(book);
      }

      // GET: Book/Delete/5
      public ActionResult Delete(int? id) {
         if (id == null) {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
         }
         Book book = db.Books.Find(id);
         if (book == null) {
            return HttpNotFound();
         }
         return View(book);
      }

      // POST: Book/Delete/5
      [HttpPost, ActionName("Delete")]
      [ValidateAntiForgeryToken]
      public ActionResult DeleteConfirmed(int id) {
         Book book = db.Books.Find(id);
         db.Books.Remove(book);
         db.SaveChanges();
         return RedirectToAction("Index");
      }

      protected override void Dispose(bool disposing) {
         if (disposing) {
            db.Dispose();
         }
         base.Dispose(disposing);
      }
   }
}
